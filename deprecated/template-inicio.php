<?php
/* 
Template Name: Plantilla Inicio
*/
get_header();
?>
<div class="u-full-width u-max-full-width portada">

	<?php
	if(have_posts()):
	 	while(have_posts()) : the_post();
			the_content();
		endwhile;
	else:	
		echo '<p>No se encontro contenido</p>';
	endif;
	?>

</div> <!-- end contact-us wraper -->
<?php
	wp_footer();
?>