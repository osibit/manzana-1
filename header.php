<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php bloginfo('name'); ?></title>
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
		<!-- site-header -->
		<header class="u-full-width">
			<div class="container">
				<div class="row">
					<?php if (is_page('inicio')){ ?>
						Aqui va el menu de Inicio
					<?php } else{ ?>
					<div class="three columns">
						<div id="logo-manzana">
						</div>
					</div> <!-- end logo -->

					<nav class="nine columns" id="top-menu">
							<?php
								$args = array(
									'theme_location' => 'primary'
								);
							?>
							<?php wp_nav_menu($args); ?>
						<!-- </a> -->
					</nav><!-- end Navigation -->

					<select class="mobile-nav" onChange="window.location.href=this.value">
						<option value="index.html">Menu</option>
						<option value="./">Inicio</option>
						<option value="#">Exposiciones</option>
						<option value="#">Simposio de Escultores</option>
						<option value="#">Amigos y Auspiciadores</option>
						<option value="#">Novedades</option>
						<option value="#">Contacto</option>
						<option value="#">Buscar</option>
					</select> <!-- end mobile-nav -->
					<?php } ?>

				</div> <!-- end ROW -->
			</div><!-- end header-container -->
		</header>
		<!-- end site-header -->
