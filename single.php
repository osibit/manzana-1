<?php get_header(); ?>
	<section class="u-full-width single-post">
		<div class="container">
			<?php
				if(have_posts()):
					while(have_posts()) : the_post();?>
						<div class="row post">
							<div class="twelve columns">
								<article>
									<h2><?php the_title(); ?></h2>
									<?php the_content(); ?>
								</article>
							</div>
						</div>
			<?php 
				endwhile;
				else:
					echo '<p>No se encontro contenido</p>';
				endif;
			?>
		</div>
	</section>
<?php	get_footer();
?>