
	<footer class="u-full-width">
		<div class="container">
			<div class="row">
				<div class="three columns">
					<div class="copyright">&copy; <?php echo date('Y'); ?> - <?php bloginfo('name'); ?></div>
				</div>
				<div class="nine columns">
					<nav id="menu-social">
						<?php
							$args = array(
								'theme_location' => 'footer'
							);
						?>
						<?php wp_nav_menu($args); ?>
					</nav><!-- end navigation-footer -->
				</div>
			</div>
		</div>
	</footer> <!-- end footer -->
	<?php wp_footer(); ?>
	</body>
</html>