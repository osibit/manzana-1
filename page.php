<?php
get_header();
if (have_posts()) :
	while(have_posts()) : the_post(); ?>

		<div class="container page-content"> <!-- content-container -->
			<div cla
				<div class="three columns left-sidebar">
					<nav class="sidebar">
						<ul>
							<?php
								$args = array(
									'child_of' => get_top_ancestor_id(),
									'title_li' => ''
									);
							?>
							<?php wp_list_pages($args); ?>
						</ul>
					</nav>
				</div> <!-- end sidebar -->
				<div class="nine columns main-content">
					<article>
						<?php the_content(); ?>
					</article>
				</div> <!-- end main-content -->
				<div class="three columns right-sidebar">
					<nav class="sidebar">
						<ul>
							<?php
								$args = array(
									'child_of' => get_top_ancestor_id(),
									'title_li' => ''
									);
							?>
							<?php wp_list_pages($args); ?>
						</ul>
					</nav>
				</div>
			</div> <!-- end content-row -->
		</div> <!-- end container -->

	<?php endwhile;

	else:
		echo '<p>No se encontro contenido.</p>';
	endif;
get_footer();
?>